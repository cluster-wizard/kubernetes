
## Initial deployment

- Tutorial : [Kubernetes Deployment using Kubespray](https://daegonk.medium.com/kubernetes-deployment-using-kubespray-63e5086237f7)
- Example Files : example-user1/inventory.yaml and example-user1/ssh-config
   - 3 x node : user1-vm1 (192.168.5.13), user1-vm2 (192.168.5.14), user1-vm3 (192.168.5.15)
   - master node : user1-vm1
   - worker nodes : user1-vm2, user1-vm3
   - login credentials : useradmin / password
- Request for free VMs : [Cluster Wizard Release Project](https://gitlab.com/cluster-wizard/release/-/issues)
   - gitlab.com account is required
   - Use free-vm-request issue template